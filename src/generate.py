__author__ = "Jeremy Nelson"
import logging
import pathlib

from jinja2 import Environment, FileSystemLoader

public = pathlib.Path("public")

loader = FileSystemLoader("src/templates")

logger = logging.getLogger(__name__)

template_env = Environment(loader=loader)


def generate_website():
    home_template = template_env.get_template("index.html")

    with (public / "index.html").open("w+") as fo:
        fo.write(home_template.render())
    logger.info("Finished generating index.html")

if __name__ == "__main__":
    logger.info("Starting website generation")
    generate_website()
